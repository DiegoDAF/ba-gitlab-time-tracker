#!/bin/bash
#
tmpdir=$(mktemp -d -t gtt-daf-$(date +%Y-%m-%d-%H-%M-%S)-XXXXXXXXXX)

#PSQLOPS=" --dbname=gtt  -U postgres"
PSQLOPS=" --quiet --dbname=gtt --echo-hidden --tuples-only --no-align -U postgres --no-psqlrc --host=127.0.0.1 --port=50215 "
PSQLOPS=" --quiet --dbname=gtt --echo-hidden --tuples-only --no-align -U postgres --no-psqlrc --host=127.0.0.1 "

psql $PSQLOPS -c " select project_id::varchar || ',' || description from projects where estado = 'AA' order by random();" --output="$tmpdir/allprojects.lst"

if test -f "$tmpdir/allprojects.lst"; then
    while IFS=, read -r current_project current_description
    do
    
        echo "$(date +"%Y-%m-%d %H:%M:%S") - PARENT: $current_project - $current_description"
        bash gettime.sh $current_project ## 2>&1 >/dev/null

    done <  "$tmpdir/allprojects.lst"
fi

# clear

# PSQLOPS3=" --quiet --dbname=gtt --echo-hidden -U postgres --no-psqlrc --host=127.0.0.1"
# psql $PSQLOPS3 -f yo.sql

clear

PSQLOPS3=" --quiet --dbname=gtt --echo-hidden -U postgres --no-psqlrc --host=127.0.0.1"

psql $PSQLOPS3 -f yoc1.sql -o /tmp/yoc1.out 
psql $PSQLOPS3 -f yoc2.sql -o /tmp/yoc2.out 
psql $PSQLOPS3 -f yoc3.sql -o /tmp/yoc3.out

paste /tmp/yoc1.out /tmp/yoc2.out /tmp/yoc3.out