-- DROP TABLE IF EXISTS public.projects;

CREATE TABLE IF NOT EXISTS public.projects
(
    id integer NOT NULL DEFAULT nextval('projects_id_seq'::regclass),
    proyect_id integer NOT NULL,
    description character varying(50) COLLATE pg_catalog."default",
    estado character(2) COLLATE pg_catalog."default" NOT NULL DEFAULT 'AA'::bpchar,
    CONSTRAINT projects_pkey PRIMARY KEY (proyect_id)
)
-- DROP TABLE IF EXISTS public.spent;

CREATE TABLE IF NOT EXISTS public.spent
(
    id serial NOT NULL,
	proyect_id integer NOT NULL,
    issue_id integer NOT NULL,
    fecha timestamp without time zone NOT NULL,
    usuario character varying(50) COLLATE pg_catalog."default" NOT NULL,
    spent interval NOT NULL,
    CONSTRAINT spent_pkey PRIMARY KEY (proyect_id, issue_id, fecha, usuario)
);

 INSERT INTO public.spent(proyect_id, issue_id, fecha, usuario, spent) VALUES (
	 25153192, 8, '2021-09-17T21:29:40.181Z', 'gerardo.herzig', '2h') on conflict do nothing;

tr u ncate table spent
select count(*) from spent
select * from spent where random() > 0.01 limit 10;

		select date_trunc('day', fecha), sum(spent)
		from spent
		where usuario = 'DiegoDAF'
		  and fecha > '20220130'
		group by date_trunc('day', fecha) 
		order by date_trunc('day', fecha) desc

		select date_trunc('month', fecha), sum(spent)
		from spent
		where proyect_id = 25153192
		  and fecha > '20211101'
		group by date_trunc('month', fecha) 
		order by date_trunc('month', fecha) desc

		select spent.project_id, projects.description, date_trunc('month', spent.fecha), sum(spent.spent)
		from spent 
		left join projects on projects.project_id = spent.project_id
		where fecha > '20220101'
		group by date_trunc('month', spent.fecha), spent.project_id , projects.description
		order by projects.description, date_trunc('month', spent.fecha) desc


		select date_trunc('month', fecha), usuario, sum(spent)
		from spent
		where proyect_id = 25153192
		  and fecha > '20211101'
		group by date_trunc('month', fecha) , usuario
		order by date_trunc('month', fecha) desc, usuario




select replace ( ((now() - '5 days'::interval) AT TIME ZONE 'UTC')::varchar, ' ' , 'T') as desde

	select date_trunc('day', fecha), sum(spent)
		from spent
		where usuario = 'DiegoDAF'
		  and fecha > '20220130'
		group by date_trunc('day', fecha) 
		order by date_trunc('day', fecha) desc

        create view mytime as
		select spent.project_id, projects.description, issue_id, date_trunc('day', spent.fecha), sum(spent.spent)
		from spent 
		left join projects on projects.project_id = spent.project_id
		where fecha > '20220130'
          and usuario = 'DiegoDAF'
		group by date_trunc('day', spent.fecha), spent.project_id , projects.description,issue_id
		order by date_trunc('day', spent.fecha) desc, projects.description,issue_id
        
        select date_trunc('day', spent.fecha), sum(spent.spent)
		from spent 
		left join projects on projects.project_id = spent.project_id
		where fecha > '20220130'
          and usuario = 'DiegoDAF'
		group by date_trunc('day', spent.fecha)
		order by date_trunc('day', spent.fecha) desc
        
        
        
        