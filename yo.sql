/*
select a.description, tmp.date_trunc, a.horas, coalesce(tmp.spent, '00:00:00'::interval) spent
from projects a left join (
    select spent.project_id, projects.description, date_trunc('month', spent.fecha), sum(spent.spent) spent, projects.horas
    from spent 
    left join projects on projects.project_id = spent.project_id
    where fecha > date_trunc('month', now() )
    group by date_trunc('month', spent.fecha), spent.project_id , projects.description, projects.horas
    ) as tmp on tmp.project_id = a.project_id
order by spent    ;

select * from projects limit 10
*/
/*
-- horas por semana y cliente
select projects.description, to_char(date_trunc('week', spent.fecha), 'IYYY-IW'), sum(spent.spent)
from spent 
left join projects on projects.project_id = spent.project_id
where fecha > now() - '10 weeks'::interval
	--and usuario = 'DiegoDAF'
group by to_char(date_trunc('week', spent.fecha), 'IYYY-IW'), projects.description
order by to_char(date_trunc('week', spent.fecha), 'IYYY-IW') desc, projects.description
limit 15
;
*/


/*
-- horas por cliente para el mes actual
select case when a.description ilike 'stcpa%' then 'STCPay' else a.description end Client,  sum(coalesce(tmp.spent, '00:00:00'::interval)) spent, 
    max(case when a.horas is not null then
        a.horas - tmp.spent
    else
        '00:00:00'::interval
    end) pending
    --, a.horas total
from projects a left join (
        select spent.project_id, projects.description, date_trunc('month', spent.fecha), sum(spent.spent) spent
        from spent 
    left join projects on projects.project_id = spent.project_id
    where fecha > date_trunc('month', now() )
    group by date_trunc('month', spent.fecha), spent.project_id , projects.description
    ) as tmp on tmp.project_id = a.project_id
where spent <> '00:00:00'::interval
group by case when a.description ilike 'stcpa%' then 'STCPay' else a.description end
order by spent    ;
*/

/* -- horas extra
select   p.description, s.* 
from spent s
left join projects p on p.project_id = s.project_id
where usuario = 'DiegoDAF'
and fecha between '20220201' and '20220301'
and (  EXTRACT(ISODOW FROM fecha) IN (6, 7)
     or extract(hour from fecha) > 18 ) --
order by fecha
*/

-- Detalle horas diego 
select spent.project_id, projects.description, issue_id, date_trunc('day', spent.fecha), sum(spent.spent)
from spent 
left join projects on projects.project_id = spent.project_id
where fecha > now() - '3 days'::interval
	and usuario = 'DiegoDAF'
group by date_trunc('day', spent.fecha), spent.project_id , projects.description,issue_id
order by date_trunc('day', spent.fecha) asc, projects.description,issue_id;
  
select --min(spent.project_id) project_id, 
    case when projects.description ilike 'stcpa%' then 'STCPay' else projects.description end Client,
    --date_trunc('month', spent.fecha), 
    --right('                        ' || justify_hours(sum(spent.spent))::text ,17) spent,
    
    right('                        ' || TO_CHAR(sum(spent.spent), 'fmHH24hfmMIm') ,12) spent, 
     
    --projects.horas,
    
    --right('                        ' || justify_hours((projects.horas -  sum(spent.spent)))::char(20) ,17) pending
    right('                        ' ||   TO_CHAR(projects.horas -  sum(spent.spent), 'fmHH24hfmMIm') ,12) pending
    
from spent 
left join projects on projects.project_id = spent.project_id
where fecha > date_trunc('month', now() )
group by date_trunc('month', spent.fecha), 
    case when projects.description ilike 'stcpa%' then 'STCPay' else projects.description end,
    projects.horas
order by Client;

-- insert into  projects(project_id, description, horas) select 38250550, 'AgriData', '10:00:00'::interval
-- update projects set horas = '160:00:00'::interval where id = 10

-- select * from projects order by description


-- horas por dia diego
select date_trunc('day', spent.fecha), to_char(date_trunc('day', spent.fecha), 'DAY') AS "Day", sum(spent.spent)
from spent 
left join projects on projects.project_id = spent.project_id                                                                                                   
where fecha >  now() - '10 days'::interval
  and usuario = 'DiegoDAF'                                                                                                                                           
 group by date_trunc('day', spent.fecha)                                                                                                                        
order by date_trunc('day', spent.fecha) desc        ;
