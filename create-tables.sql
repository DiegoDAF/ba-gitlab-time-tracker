
CREATE TABLE IF NOT EXISTS public.spent
(
    id integer NOT NULL DEFAULT nextval('spent_id_seq'::regclass),
    project_id integer NOT NULL,
    issue_id integer NOT NULL,
    fecha timestamp without time zone NOT NULL,
    usuario character varying(50) COLLATE pg_catalog."default" NOT NULL,
    spent interval NOT NULL,
    estado character varying(2) COLLATE pg_catalog."default" NOT NULL DEFAULT 'AA'::character varying,
    created_at timestamp without time zone NOT NULL DEFAULT now(),
    comment text COLLATE pg_catalog."default",
    CONSTRAINT spent_pkey PRIMARY KEY (project_id, issue_id, fecha, usuario)
);

CREATE TABLE IF NOT EXISTS public.projects
(
    id integer NOT NULL DEFAULT nextval('projects_id_seq'::regclass),
    project_id integer NOT NULL,
    description character varying(50) COLLATE pg_catalog."default",
    estado character(2) COLLATE pg_catalog."default" NOT NULL DEFAULT 'AA'::bpchar,
    CONSTRAINT projects_pkey PRIMARY KEY (project_id)
);