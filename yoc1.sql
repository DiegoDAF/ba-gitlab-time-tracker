select date_trunc('day', spent.fecha), to_char(date_trunc('day', spent.fecha), 'DAY') AS "Day", sum(spent.spent)
from spent 
left join projects on projects.project_id = spent.project_id                                                                                                   
where fecha >  now() - '10 days'::interval
  and usuario = 'DiegoDAF'                                                                                                                                           
 group by date_trunc('day', spent.fecha)                                                                                                                        
order by date_trunc('day', spent.fecha) desc        ;