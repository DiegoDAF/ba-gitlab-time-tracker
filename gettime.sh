#!/bin/bash
#
#
#
#  /usr/lib/postgresql/14/bin/initdb -E=UTF-8 -U postgres -A trust -W  --data-checksums -D /home/daf/temp/gtt
#  /usr/lib/postgresql/14/bin/pg_ctl -D /home/daf/temp/gtt -l logfile start 
#


PRIVATE_TOKEN=$(grep "^token:" ~/.local/share/.gtt/config2.yml | sed 's/token: //')
BASE_URL=$(grep "^url:" ~/.local/share/.gtt/config2.yml | sed 's/url: //')
project_id=$1
tmpdir=$2
DIAS="10 days"
fecha10=$(date --date="$DIAS ago" +"%Y-%m-%d %H:%M:%S")


PSQLOPS=" --quiet --dbname=gtt --echo-hidden --tuples-only --no-align -U postgres --no-psqlrc -h 127.0.0.1 -p 50215 "
PSQLOPS2=" --quiet --dbname=gtt --echo-hidden --tuples-only --no-align -U postgres --no-psqlrc -h 127.0.0.1 "

PSQLOPS=$PSQLOPS2

LAST35=$(psql $PSQLOPS -c "select replace ( ((now() - '$DIAS'::interval) AT TIME ZONE 'UTC')::varchar, ' ' , 'T') as desde")

[[ -z "${VAR}" ]] && echo -n "." || echo "$(date +"%Y-%m-%d %H:%M:%S") - Token: ${PRIVATE_TOKEN:1:3}...${PRIVATE_TOKEN:(-3)} "
[[ -z "${VAR}" ]] && echo -n "." || echo "$(date +"%Y-%m-%d %H:%M:%S") - Working dir: $tmpdir"

if [ -z "$tmpdir" ]
then
      [[ -z "${VAR}" ]] && echo -n "." || echo "$(date +"%Y-%m-%d %H:%M:%S") - generating temp fodler"
      tmpdir=$(mktemp -d -t gtt-daf-$(date +%Y-%m-%d-%H-%M-%S)-XXXXXXXXXX)
else
      [[ -z "${VAR}" ]] && echo -n "." || echo "$(date +"%Y-%m-%d %H:%M:%S") - using temp fodler"
fi
[[ -z "${VAR}" ]] && echo -n "." || echo "$(date +"%Y-%m-%d %H:%M:%S") - TempFodler: $tmpdir"

# for each project_id
[[ -z "${VAR}" ]] && echo -n "." || echo "$(date +"%Y-%m-%d %H:%M:%S") - Current proyect $project_id"

# TODO filter by modified date

for ((i=1;i<=5;i++));
do

    curl --silent --header "PRIVATE-TOKEN: $PRIVATE_TOKEN" "${BASE_URL}projects/${project_id}/issues?per_page=100&page=$i&updated_after=$LAST35" | jq . >  "$tmpdir/$project_id.$i.json"

done

#echo "$(date +"%Y-%m-%d %H:%M:%S") - files in queue"
#ls -lh $tmpdir

# TODO filter by modified date
[[ -z "${VAR}" ]] && echo -n "." || echo "$(date +"%Y-%m-%d %H:%M:%S") - merging jsons"
jq -r .[].iid  $tmpdir/$project_id.[0-9].json | grep -iv null >  "$tmpdir/$project_id.x.json"


# find all notes ssue_iid
[[ -z "${VAR}" ]] && echo -n "." || echo "$(date +"%Y-%m-%d %H:%M:%S") - Gathering notes from issue_iid: "
while read issue_iid; do
    [[ -z "${VAR}" ]] && echo -n "." || echo -n "$issue_iid "
    [ -d "$tmpdir/$project_id" ] || mkdir -p "$tmpdir/$project_id"

    for ((i=1;i<=5;i++));
    do
    
        curl --silent --header "PRIVATE-TOKEN: $PRIVATE_TOKEN" "${BASE_URL}projects/${project_id}/issues/$issue_iid/notes?sort=asc&order_by=created_at&updated_after=$LAST35&per_page=300&page=$i" |  jq . >  "$tmpdir/$project_id/$issue_iid.$i.json"

    done
    
done <  "$tmpdir/$project_id.x.json"



[[ -z "${VAR}" ]] && echo -n "." || echo "done."
[[ -z "${VAR}" ]] && echo -n "." || echo "$(date +"%Y-%m-%d %H:%M:%S") - finding added time."

    if [ -d "$tmpdir/$project_id" ]; then

        # extracting all the times in one file, csv like for added
        jq --raw-output '.[] | "\"\(.noteable_iid)\",\"\(.author.username)\",\"\(.body)\",\"\(.created_at)\",\"\(.id)\""' "$tmpdir/$project_id"/*.json | grep -i -E "\"added " | grep -i -E "of time spent" > "$tmpdir/$project_id/allspent.tmp" 2>/dev/null

    fi

    if [ -f "$tmpdir/$project_id/allspent.tmp" ]; then 

        sort -u "$tmpdir/$project_id/allspent.tmp" > "$tmpdir/$project_id/allspent.lst" 2> /dev/null

        [[ -z "${VAR}" ]] && echo -n "." || echo "$(date +"%Y-%m-%d %H:%M:%S") - Saving in pg."

        if test -f "$tmpdir/$project_id/allspent.lst"; then
            #put every in variables 
            while IFS=, read -r noteable_iid username tiempo fecha entryid
            do

                #date manipulation to find the comment in the previous item
                # 1 to skip quotes""
                tmpFecha=${fecha:1:17}
                
                tiempo=$(sed -e 's/^"//' -e 's/"$//' <<<"$tiempo")

                #in case of fecha is not created at:
                if [[ "$tiempo" == *" at "* ]]; then
                    [[ -z "${VAR}" ]] && echo -n "." || echo -n "$(date +"%Y-%m-%d %H:%M:%S") -           adjusting date from $fecha to "
                    arrin=(${fecha//T/ })
                    fecha="$(echo "$tiempo" | sed 's/.* at //') ${arrin[1]}"
                    [[ -z "${VAR}" ]] && echo -n "." || echo "$fecha"
                fi

                tiempo=${tiempo//added /}
                var=$(echo "$tiempo" | grep -o -b "of time spent" | head -1)
                pos="${var%:*}"
                tiempo=${tiempo:0:($pos) -1}

                noteable_iid=$(sed -e 's/^"//' -e 's/"$//' <<<"$noteable_iid")
                fecha=$(sed -e 's/^"//' -e 's/"$//' <<<"$fecha")
                username=$(sed -e 's/^"//' -e 's/"$//' <<<"$username")

                entrycomment=" "
                jq '.[] | select(.created_at>"'$tmpFecha'") | select(.id<'$entryid') | .body ' $tmpdir/$project_id/$noteable_iid.*.json | head -n 1 > $tmpdir/tmpcomment.out
                
                #entrycomment=$(cat $tmpdir/tmpcomment.out)
                entrycomment=" "

                fechaIN=$(date +"%Y-%m-%d %H:%M:%S" --date="$fecha" )
                if [[ $fecha10 < $fechaIN ]] ; then

                    [[ -z "${VAR}" ]] && echo -n "." || echo "$(date +"%Y-%m-%d %H:%M:%S") - $project_id ----- $noteable_iid ----- $fecha ----- $username ----- $tiempo  ----- $entryid ----- "   

                    psql $PSQLOPS  -c "INSERT INTO public.spent(project_id, issue_id, fecha, usuario, spent, comment) VALUES ($project_id, $noteable_iid, '$fecha', '$username', '$tiempo', '${entrycomment//\'/''}') on conflict do nothing;"
                    # psql $PSQLOPS2 -c "INSERT INTO public.spent(project_id, issue_id, fecha, usuario, spent, comment) VALUES ($project_id, $noteable_iid, '$fecha', '$username', '$tiempo', '${entrycomment//\'/''}') on conflict do nothing;"

                else

                    echo -n "."

                fi

            done < "$tmpdir/$project_id/allspent.lst"
        fi

    fi

    if [ -d "$tmpdir/$project_id" ]; then
        # for the subtracted... pay attention to symbol - in $tiempo en the insert.
        jq --raw-output '.[] | "\"\(.noteable_iid)\",\"\(.author.username)\",\"\(.body)\",\"\(.created_at)\""' "$tmpdir/$project_id"/*.json | grep -i -E "\"subtracted " | grep -i -E "of time spent" > "$tmpdir/$project_id/allsubtracted.tmp" 2>/dev/null
        fi

    if [ -f "$tmpdir/$project_id/allsubtracted.tmp" ]; then 

        sort -u "$tmpdir/$project_id/allsubtracted.tmp" > "$tmpdir/$project_id/allsubtracted.lst" 2>/dev/null

        if test -f "$tmpdir/$project_id/allsubtracted.lst"; then
            #put every in variables 
            while IFS=, read -r noteable_iid username tiempo fecha
            do

                tiempo=$(sed -e 's/^"//' -e 's/"$//' <<<"$tiempo")
                tiempo=${tiempo//subtracted /}
                var=$(echo "$tiempo" | grep -o -b "of time spent" | head -1)
                pos="${var%:*}"
                tiempo=${tiempo:0:($pos) -1}

                noteable_iid=$(sed -e 's/^"//' -e 's/"$//' <<<"$noteable_iid")
                fecha=$(sed -e 's/^"//' -e 's/"$//' <<<"$fecha")
                username=$(sed -e 's/^"//' -e 's/"$//' <<<"$username")

                fechaIN=$(date +"%Y-%m-%d %H:%M:%S" --date="$fecha" )
                if [[ $fecha10 < $fechaIN ]] ; then

                    [[ -z "${VAR}" ]] && echo -n "." || echo "$(date +"%Y-%m-%d %H:%M:%S") - $project_id ----- $noteable_iid ----- $fecha ----- $username ----- $tiempo"   

                    psql $PSQLOPS -c "INSERT INTO public.spent(project_id, issue_id, fecha, usuario, spent) VALUES ($project_id, $noteable_iid, '$fecha', '$username', '-$tiempo') on conflict do nothing;"
                    # psql $PSQLOPS2 -c "INSERT INTO public.spent(project_id, issue_id, fecha, usuario, spent) VALUES ($project_id, $noteable_iid, '$fecha', '$username', '-$tiempo') on conflict do nothing;"

                else

                    echo -n "."

                fi

            done < "$tmpdir/$project_id/allsubtracted.lst"
        fi
    fi


    if [ -d "$tmpdir/$project_id" ]; then
        # for the subtracted... pay attention to symbol - in $tiempo en the insert.
        jq --raw-output '.[] | "\"\(.noteable_iid)\",\"\(.author.username)\",\"\(.body)\",\"\(.created_at)\""' "$tmpdir/$project_id"/*.json |  grep -i -E "\"removed time spent\"" > "$tmpdir/$project_id/allremoved.tmp" 2>/dev/null
        fi

    if [ -f "$tmpdir/$project_id/allremoved.tmp" ]; then 

        sort -u "$tmpdir/$project_id/allremoved.tmp" > "$tmpdir/$project_id/allremoved.lst" 2>/dev/null

        if test -f "$tmpdir/$project_id/allremoved.lst"; then
            #put every in variables 
            while IFS=, read -r noteable_iid username tiempo fecha
            do

                #tiempo=$(sed -e 's/^"//' -e 's/"$//' <<<"$tiempo")
                #tiempo=${tiempo//subtracted /}
                #var=$(echo "$tiempo" | grep -o -b "of time spent" | head -1)
                #pos="${var%:*}"
                #tiempo=${tiempo:0:($pos) -1}

                noteable_iid=$(sed -e 's/^"//' -e 's/"$//' <<<"$noteable_iid")
                fecha=$(sed -e 's/^"//' -e 's/"$//' <<<"$fecha")
                username=$(sed -e 's/^"//' -e 's/"$//' <<<"$username")

                [[ -z "${VAR}" ]] && echo -n "." || echo "$(date +"%Y-%m-%d %H:%M:%S") - $project_id ----- $noteable_iid ----- $fecha ----- $username ----- $tiempo"   

                fechaIN=$(date +"%Y-%m-%d %H:%M:%S" --date="$fecha" )
                if [[ $fecha10 < $fechaIN ]] ; then

                    psql $PSQLOPS -c "update public.spent set estado = 'AZ' where project_id = $project_id and issue_id = $noteable_iid and fecha < '$fecha';"
                    # psql $PSQLOPS2 -c "update public.spent set estado = 'AZ' where project_id = $project_id and issue_id = $noteable_iid and fecha < '$fecha';"

                else

                    echo -n "."

                fi

            done < "$tmpdir/$project_id/allremoved.lst"
        fi
    fi

echo " "
echo "$(date +"%Y-%m-%d %H:%M:%S") - done."
echo " "
