-- Detalle horas diego 
select spent.project_id, projects.description, issue_id, date_trunc('day', spent.fecha), sum(spent.spent)
from spent 
left join projects on projects.project_id = spent.project_id
where fecha > now() - '3 days'::interval
	and usuario = 'DiegoDAF'
group by date_trunc('day', spent.fecha), spent.project_id , projects.description,issue_id
order by date_trunc('day', spent.fecha) asc, projects.description,issue_id;