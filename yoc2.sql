select --min(spent.project_id) project_id, 
    case when projects.description ilike 'stcpa%' then 'STCPay' else projects.description end Client,
    --date_trunc('month', spent.fecha), 
    --right('                        ' || justify_hours(sum(spent.spent))::text ,17) spent,
    
    right('                        ' || TO_CHAR(sum(spent.spent), 'fmHH24hfmMIm') ,12) spent, 
     
    --projects.horas,
    
    --right('                        ' || justify_hours((projects.horas -  sum(spent.spent)))::char(20) ,17) pending
    right('                        ' ||   TO_CHAR(projects.horas -  sum(spent.spent), 'fmHH24hfmMIm') ,12) pending
    
from spent 
left join projects on projects.project_id = spent.project_id
where fecha > date_trunc('month', now() )
group by date_trunc('month', spent.fecha), 
    case when projects.description ilike 'stcpa%' then 'STCPay' else projects.description end,
    projects.horas
order by Client;