PSQLOPS=" --quiet --dbname=gtt --echo-hidden --tuples-only --no-align -U postgres --no-psqlrc --host=127.0.0.1 "

PRIVATE_TOKEN=$(grep "^token:" ~/.local/share/.gtt/config2.yml | sed 's/token: //')
BASE_URL=$(grep "^url:" ~/.local/share/.gtt/config2.yml | sed 's/url: //')

#echo "PRIVATE_TOKEN: $PRIVATE_TOKEN"
#echo "BASE_URL     : $BASE_URL"

#NOTE_STRING=https://gitlab.ongres.com/ongresportal/stcpay/stcpay/-/issues/139#note_42776
#NOTE_STRING=https://gitlab.ongres.com/ongresportal/stcpay/stcpay/-/issues/139#note_42642

NOTE_STRING=$1

echo "NOTE_STRING  : $NOTE_STRING"

issue_iid=$(echo $NOTE_STRING | cut -f9 -d/ | awk 'BEGIN {FS="#";} { print $1}' )
note_id=$(echo $NOTE_STRING | cut -f9 -d/ | awk 'BEGIN {FS="#";} { print $2}' | awk 'BEGIN {FS="_";} { print $2}'  )

echo "issue_iid    : $issue_iid"
echo "note_id      : $note_id"

project_id1=$(echo $NOTE_STRING | cut -f6 -d/)
project_id=$(psql $PSQLOPS -c "select project_id from projects where description ilike '$project_id1' limit 1;")

echo "project_id   : $project_id"


tmpdir=$(mktemp -d -t gtt-daf-$(date +%Y-%m-%d-%H-%M-%S)-XXXXXXXXXX)
tmpfile="$tmpdir/$project_id.$issue_iid.$note_id.json"

#echo "tmpdir       : $tmpdir"
#echo "tmpfile      : $tmpfile"

if [ -f "$tmpfile" ] ; then
    echo "   removed!"
    rm "$tmpfile"
fi

curl --silent --header "PRIVATE-TOKEN: $PRIVATE_TOKEN" \
     "${BASE_URL}projects/${project_id}/issues/$issue_iid/notes/$note_id" | \
     jq . >  "$tmpfile"


body=$(sed  -e "s/'//" <<<"$(jq -r .body "$tmpfile")" )
echo "bodylen      : ${#body}"

#next comment, find spend
((note_id=note_id+1))

curl --silent --header "PRIVATE-TOKEN: $PRIVATE_TOKEN" \
     "${BASE_URL}projects/${project_id}/issues/$issue_iid/notes/$note_id" | \
     jq . >  "$tmpfile.spend"

var1=$(jq -r .body "$tmpfile.spend") 
var2=${var1//added /}
spentinterval=${var2/ of time spent/}

#in case of fecha is not created at:
if [[ "$spentinterval" == *" at "* ]]; then
     echo "   adjusting date..."
     arrin=(${spentwhen//T/ })
     
     spentwhen="$(echo "$spentinterval" | sed 's/.* at //') ${arrin[1]}"
     spentinterval=$(echo "$spentinterval" | sed 's/ at.*//')

else
     spentwhen=$(jq -r .created_at "$tmpfile.spend")      
fi

echo "spentinterval: $spentinterval"
echo "spentwhen    : $spentwhen"

kubectl config use-context arn:aws:eks:us-east-1:857517990941:cluster/ongres-cluster
sleep 2
kubectl port-forward -n pstm pstm-db-0 50215:5432 >/dev/null 2>&1 & 

PSQLOPS1=" --quiet --dbname=pstm_db --echo-hidden --tuples-only --no-align -U postgres --no-psqlrc --host=127.0.0.1 --port=50215 "

# temp
#project_id2=$(psql $PSQLOPS1 -c "SELECT id FROM pstm.project where id_customer = (SELECT id FROM pstm.customer where name ilike '$project_id1' limit 1);")

project_id2=$(psql $PSQLOPS1 -c "select id from (SELECT id, 0 w FROM pstm.project where id_customer = (SELECT id FROM pstm.customer where name ilike '$project_id1' limit 1) union select 60, 1 order by w) tmp limit 1 ;")


#  id          | bigint                      |           | not null |         | plain    |             |              | 
#  comment     | text                        |           |          |         | extended |             |              | 
#  issueurl    | text                        |           |          |         | extended |             |              | 
#  starthour   | timestamp without time zone |           |          |         | plain    |             |              | 
#  finishhour  | timestamp without time zone |           |          |         | plain    |             |              | 
#  totalhours  | real                        |           |          |         | plain    |             |              | 
#  billedhours | real                        |           |          |         | plain    |             |              | 
#  id_engineer | bigint                      |           |          |         | plain    |             |              | 
#  id_project  | bigint                      |           |          |         | plain    |             |              |

qty="insert into pstm.hourentry (id, comment, issueurl, starthour,  totalhours, billedhours, id_engineer, id_project) select nextval('pstm.hourentry_seq') , '${body}', '${NOTE_STRING}', '${spentwhen}', EXTRACT(epoch FROM '${spentinterval}'::interval)/3600, EXTRACT(epoch FROM '${spentinterval}'::interval)/3600,13, $project_id2 returning id;"      

echo " "
echo "------------------------------------"
echo "qty: $qty"
echo "------------------------------------"
echo " "

read -p "Press [Enter] key to continue..."

psql $PSQLOPS1 -c "$qty"

bash gettime.sh $project_id

clear

PSQLOPS3=" --quiet --dbname=gtt --echo-hidden -U postgres --no-psqlrc --host=127.0.0.1"

psql $PSQLOPS3 -f yoc1.sql -o /tmp/yoc1.out 
psql $PSQLOPS3 -f yoc2.sql -o /tmp/yoc2.out 
psql $PSQLOPS3 -f yoc3.sql -o /tmp/yoc3.out

paste /tmp/yoc1.out /tmp/yoc2.out /tmp/yoc3.out













